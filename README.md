json.demand.parseR
==================
Parses JSON on demand, keeping memory usage low. This was developed in-house as reading a complete JSON file was very memory demanding while not all data was used at once. 


This library is currently not yet available on CRAN, but can be simply installed by the following commands:
```bash
git clone git@bitbucket.org:omnigen/json-demand-parser.git
cd ..
R CMD build json-demand-parser
R -e 'install.packages("json.demand.parseR_1.0.tar.gz", repos=NULL)'
```

Maintainer
==========
This repository is maintained by Omnigen BV, a company based in Rotterdam (the Netherlands).
